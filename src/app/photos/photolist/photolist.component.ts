import { Component, OnInit } from '@angular/core';
import { Photo } from 'src/app/models/photo.model';
import { Project } from 'src/app/models/project.model';
import { StorageMap } from '@ngx-pwa/local-storage';
import { PdfService } from 'src/app/services/pdf.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-photolist',
  templateUrl: './photolist.component.html',
  styleUrls: ['./photolist.component.css'],
})
export class PhotolistComponent implements OnInit {
  private open: boolean = true;
  public captures: Array<Photo>;
  public imgWidth: number;
  public imgHeight: number;
  private proj: Project = new Project();

  constructor(
    protected storage: StorageMap,
    private pdfService: PdfService,
    private router: Router
  ) {
    this.captures = [];
  }

  ngOnInit() {
    console.log('Here!');
    this.storage.get('1').subscribe(<Project>(proj) => {
      if (proj) {
        this.proj = proj;
      } else {
        this.proj = new Project();
        this.storage.set('1', this.proj);
      }

      this.captures = [];
      for (var i = 0; i < this.proj.photos.length; i++) {
        var photo = this.proj.photos[i];
        var width = (window.innerWidth - 40) / 2;
        var height = (window.innerHeight - 40) / 2;
        var scale = Math.min(width / photo.width, height / photo.height);
        // photo.width = photo.width * scale;
        // photo.height = photo.height * scale;
        this.imgWidth = photo.width * scale;
        this.imgHeight = photo.height * scale;
        this.captures.push(photo);
        console.log(
          'photo width, height = ' + photo.width + ', ' + photo.height
        );
      }
    });
  }

  public ngAfterViewInit() {}

  public pdfIt() {
    this.pdfService.pdfIt();
  }

  public delete(val: any) {
    console.log('val = ' + val);
    let index = this.captures.findIndex((d) => d.id === val); //find index in your array
    this.captures.splice(index, 1); //remove element from array
    console.log('index = ' + index);
    this.proj.photos = this.captures;
    this.storage.set('1', this.proj).subscribe(() => {
      console.log('project updated!');
    });
  }
}
