import { viewClassName } from '@angular/compiler';
import {
  Component,
  OnInit,
  ViewChild,
  ElementRef,
  Renderer2,
  HostListener,
  AfterViewInit,
} from '@angular/core';
import { Router } from '@angular/router';
import { StorageMap } from '@ngx-pwa/local-storage';
import { Photo } from '../../models/photo.model';
import { Project } from '../../models/project.model';
import { PdfService } from '../../services/pdf.service';
import { BreakpointObserver, BreakpointState } from '@angular/cdk/layout';
import { Moralis } from 'moralis/dist/moralis';

@Component({
  selector: 'photo-grab',
  templateUrl: './photograb.component.html',
  styleUrls: ['./photograb.component.css'],
})
export class PhotograbComponent implements OnInit, AfterViewInit {
  private open: boolean = true;
  public captures: Array<Photo>;
  public camWidth = window.innerWidth * 0.5;
  public camHeight = window.innerHeight;
  public imgWidth: number;
  public imgHeight: number;
  winHeight;
  winWidth;
  public proj: Project = new Project();
  public value = 'Clear me';
  picspersec: number = 1;
  btnText: string = 'Start';
  disableBtn: boolean = false;
  autoMode: boolean = false;
  liveMode: boolean = false;

  @ViewChild('video', { read: ElementRef, static: true })
  public video: ElementRef;

  @ViewChild('viddiv', { read: ElementRef, static: true })
  public viddiv: ElementRef;

  mediaStream: MediaStream;
  autoIntervalId;

  constructor(
    protected storage: StorageMap,
    private pdfService: PdfService,
    private router: Router,
    private renderer: Renderer2,
    public breakpointObserver: BreakpointObserver
  ) {
    this.captures = [];
  }

  ngOnInit() {
    console.log('Here!');
    this.storage.get('1').subscribe(<Project>(proj) => {
      if (proj) {
        this.proj = proj;
      } else {
        this.proj = new Project();
        this.proj.name = 'MoveIn!';
        this.storage.set('1', this.proj);
      }

      this.captures = [];
      for (var i = 0; i < this.proj.photos.length; i++) {
        var photo = this.proj.photos[i];
        var width = (window.innerWidth - 40) / 2;
        var height = (window.innerHeight - 40) / 2;
        var scale = Math.min(width / photo.width, height / photo.height);
        photo.width = photo.width * scale;
        photo.height = photo.height * scale;
        this.imgWidth = photo.width;
        this.imgHeight = photo.height;
        this.captures.push(photo);
        console.log('capture');
      }
    });

    this.breakpointObserver
      .observe(['(min-width: 500px)'])
      .subscribe((state: BreakpointState) => {
        if (state.matches) {
          console.log(
            'Viewport width is 500px or greater! ' + window.innerWidth
          );
          this.video.nativeElement.setAttribute('width', '300px');
          this.viddiv.nativeElement.setAttribute('width', '300px');
          this.video.nativeElement.setAttribute('height', '300px');
          this.viddiv.nativeElement.setAttribute('height', '300px');
        } else {
          console.log(
            'Viewport width is less than 500px! ' + window.innerWidth
          );
          this.video.nativeElement.setAttribute('width', '250x');
          this.viddiv.nativeElement.setAttribute('width', '250px');
          this.video.nativeElement.setAttribute('height', '250px');
          this.viddiv.nativeElement.setAttribute('height', '250px');
        }
      });
  }

  @HostListener('window:resize', [])
  public onResize() {
    // this.winHeight = window.innerHeight;
    // this.winWidth = window.innerWidth;
    console.log('width2 = ' + this.winWidth);
    console.log('height2 = ' + this.winHeight);
  }

  @HostListener('window:blur', ['$event'])
  onBlur(event: any): void {
    this.stop();
  }

  public ngAfterViewInit() {
    this.winHeight = window.innerHeight;
    this.winWidth = window.innerWidth;
    console.log('width = ' + this.winWidth);
    console.log('height = ' + this.winHeight);
  }

  public pdfIt() {
    this.pdfService.pdfIt();
  }

  public clearCache() {
    this.proj.photos = [];
    this.captures = [];
    this.storage.set('1', this.proj).subscribe(() => {
      console.log('project updated!');
    });
  }

  public start() {
    console.log('picpersec = ' + this.picspersec);
    if (this.liveMode && this.autoMode) {
      this.disableBtn = true;
      this.doAuto();
    } else if (this.liveMode && !this.autoMode) {
      this.capture();
    } else {
      this.liveMode = true;
      this.doStart();
    }
  }

  private doAuto() {
    this.autoIntervalId = setInterval(() => {
      this.capture();
    }, this.picspersec * 1000);
  }

  private doStart() {
    if (navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {
      const velem: HTMLVideoElement = this.video.nativeElement;
      navigator.mediaDevices
        .getUserMedia({ audio: false, video: { facingMode: 'environment' } })
        .then((stream) => {
          this.mediaStream = stream;
          velem.srcObject = this.mediaStream;
          velem.play();
          console.log(
            'l,t = ' +
              velem.getBoundingClientRect().left +
              ', ' +
              velem.getBoundingClientRect().top
          );
          this.btnText = 'Photo';
        });
      let supoorted = navigator.mediaDevices.getSupportedConstraints();
    }
    console.log(
      'screen dimensions : ' + window.innerWidth + ', ' + window.innerHeight
    );
  }

  public stop() {
    clearInterval(this.autoIntervalId);
    try {
      this.mediaStream.getTracks().forEach((track) => {
        track.stop();
      });
      if (navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {
        // const velem: HTMLVideoElement = this.video.nativeElement;
        // velem.pause();
        // velem.removeAttribute('src');
        // velem.load();
        this.btnText = 'Start';
        this.disableBtn = false;
        this.liveMode = false;
      }
    } catch {
      console.log('stop() mediaStream fail!');
    }
  }

  public capture() {
    const velem: HTMLVideoElement = this.video.nativeElement;
    console.log('w,h = ' + velem.videoWidth + ', ' + velem.videoHeight);
    //var scale = Math.min((width / velem.videoWidth), (height / velem.videoHeight));

    var canvas = document.createElement('canvas');
    canvas.width = velem.videoWidth;
    canvas.height = velem.videoHeight;
    // var context2 = canvas.getContext('2d');
    // var img: HTMLImageElement = new Image();

    var context = canvas
      .getContext('2d')
      .drawImage(this.video.nativeElement, 0, 0, canvas.width, canvas.height);
    var imgqual = 0.92;
    var dataUrl = canvas.toDataURL('image/jpeg', imgqual);

    var photoKey = new Date().getTime() + '';
    var photo: Photo = new Photo();
    photo.id = photoKey;
    photo.dataUrl = dataUrl;
    photo.width = velem.videoWidth;
    photo.height = velem.videoHeight;
    this.imgWidth = photo.width;
    this.imgHeight = photo.height;
    photo.scale = 1;
    this.proj.photos.push(photo);
    this.storage.set('1', this.proj).subscribe(() => {
      console.log('project updated!');
    });
    this.captures.push(photo);
  }

  public delete(val: any) {
    let index = this.captures.findIndex((d) => d.id === val); //find index in your array
    this.captures.splice(index, 1); //remove element from array
    this.proj.photos = this.captures;
    this.storage.set('1', this.proj).subscribe(() => {
      console.log('project updated!');
    });
  }

  public updateAddress(e: any) {
    console.log('updateAddress() = ' + e.target.value);
    this.proj.name = e.target.value;
    this.storage.set('1', this.proj).subscribe(() => {
      console.log('project updated!');
    });
  }

  public autoChange() {
    if (!this.autoMode) {
      this.autoMode = false;
    } else {
      this.autoMode = true;
    }
  }

  public photoList() {
    this.router.navigate(['/', 'photos']);
  }

  public user: string;
  public isLoggedIn = false;
  public createdAt: string;
  public updatedAt: string;
  public ethAddress: string;
  public isAuthenticated = false;

}
