import { Injectable } from '@angular/core';
import { LocalStorage } from '@ngx-pwa/local-storage';
import { BehaviorSubject } from 'rxjs';
import { Project } from '../models/project.model';
import { User } from '../models/user.model';
import { Account } from '../models/account.model';

@Injectable({
  providedIn: 'root'
})
export class ScService {
  private currUser: User;
  private currAccount: Account;
  private currProj: Project = null;
  private sHeight: number;
  
  project_messageSource = new BehaviorSubject<Project>(null);

  public lowimgQual = .2;
  public midimgQual = .3;
  public higimgQual = .4;

  constructor(
    protected localStorage: LocalStorage,
  ) { }

  public getImgQual() {
    if (this.currProj) {
      if (this.currProj.img_qual) {
        if (this.currProj.img_qual == .75) {
          return .3;
        } else {
          return this.currProj.img_qual;
        }
      } else {
        this.currProj.img_qual = .3;
        this.localStorage.setItem(this.currProj.key, this.currProj).subscribe(() => {
          console.log("project updated!")
        });
      }
    }
  }

   public getCurrProj() {
    return this.currProj;
  }

  public setCurrProj(project: Project) {
    this.currProj = project;
  }

  public updateCurrProj() {
    this.project_messageSource.next(this.currProj);
  }

  setClientHeight(sHeight: number) {
    this.sHeight = sHeight;
  }
  setCurrAccount(_account: Account) {
    this.currAccount = _account;
  }
  setCurrUser(_user: User) {
    this.currUser = _user;
  }

}
