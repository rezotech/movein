import { AngularFireAnalytics } from '@angular/fire/analytics';
import { DatePipe } from '@angular/common';
import { Injectable } from '@angular/core';
import { StorageMap } from '@ngx-pwa/local-storage';
import { Project } from '../models/project.model';
import { Photo } from '../models/photo.model';

declare var PDFDocument: any;
declare var blobStream: any;
declare const fabric: any;

@Injectable({
  providedIn: 'root'
})
export class PdfService {
  rowCnt: number = 0;
  total_pages: number;
  total_photos: number = 0;
  curr_page: number = 1;
  exhibit_cnt: number = 0;
  proj: Project;
  xOff: number = 0;
  yOff: number = 0;
  left_margin: number = 55;
  right_margin: number = 558;
  top_margin: number = 54;
  bottom_margin: number = 738;

  private dwgmarkImage: any;
  private doc: any;

  constructor(
    protected storage: StorageMap,
    private analytics: AngularFireAnalytics,
  ) { }

  async pdfIt() {
    console.log("pdfIt!");
    this.analytics.logEvent('pdfIt!');

    this.storage.get("1").subscribe(async (proj_) => {
      this.proj = proj_ as Project;
      this.rowCnt = 0;
      this.total_pages = 0;
      this.curr_page = 1;
      this.exhibit_cnt = 1;
      this.doc = new PDFDocument({
        bufferPages: true,
        margin: 0,
        autoFirstPage: false,
        size: "letter",
        layout: "portrait"
      });
      //this.doc.info['Title'] = "MoveInReport_123.pdf";
      // 72 units per inch
      // margin should be 29
      await this.addPage(this.doc);
      console.log("photos.length = " + this.proj.photos.length);
      if (this.proj.photos.length > 0) {
        for (let photo of this.proj.photos) {
          if (this.exhibit_cnt > 6) {
            await this.addPage(this.doc);
            this.exhibit_cnt = 1;
          }
          await this.createImage(photo);
        }
      }
      const range = this.doc.bufferedPageRange();
      const end: number = range.start + range.count;
      console.log("range = " + range.start);
      console.log("count = " + range.count);
      this.doc.fontSize(12);
      for (var i = range.start; i < end; i++) {
        this.doc.switchToPage(i);
        this.doc.text("Page " + (i + 1) + " of " + end, this.right_margin - 50, this.bottom_margin);
      }
      this.close();
    });
  }

  private async addPage(doc: any) {
    doc.addPage({
      layout: "portrait",
      margin: 0
    });

    await this.addPageHeader(doc);
    await this.addPageFooter(doc);

  }

  private async addPageHeader(doc: any) {
    doc.fontSize(12);
    var string_height = doc.heightOfString("Property Address: " + this.proj.name, { width: 1000 });

    doc.fontSize(12);
    doc.text("Property Address: " + this.proj.name, this.left_margin - 10, this.top_margin - 15);

    doc.fontSize(10);
    var my_date: string = new DatePipe("en-US").transform(new Date().getTime(), 'MM/dd/yyyy');
    var string_width = doc.widthOfString("Report Date - " + my_date);
    doc.text("Report Date - " + my_date, this.right_margin - (string_width), this.top_margin - 12);
  }

  private async addPageFooter(doc: any) {
    var signatures: string = "Tenant(s): _______________________  Landlord: _______________";

    doc.fontSize(12);
    var string_height = doc.heightOfString(signatures, { width: 1000 });
    doc.text(signatures, this.left_margin - 10, this.bottom_margin);
  }

  async createImage(photo: Photo) {   // load an image function 
    // creates a new i each time it is called
    console.log("photo.width,height = " + photo.width + ", " + photo.height);
    console.log("left_margin = " + this.left_margin);
    var scale = Math.min((245 / photo.width), (200 / photo.height));
    console.log("scale = " + scale);
    this.doc.fontSize(14);
    if (this.exhibit_cnt == 1) {
      this.doc.image(photo.dataUrl, (this.left_margin - 5 + (245 - (scale * photo.width)) / 2), this.top_margin + 25, { scale: scale });
      this.doc.fontSize(12);
      // this.doc.text(scale + ", " + photo.width + ", " + photo.height, this.left_margin - 5 + (245 - (scale * photo.width)) / 2, this.top_margin + 25);
    }
    if (this.exhibit_cnt == 2) {
      this.doc.image(photo.dataUrl, (320 + (245 - (scale * photo.width)) / 2), this.top_margin + 25, { scale: scale });
    }
    if (this.exhibit_cnt == 3) {
      this.doc.image(photo.dataUrl, (this.left_margin - 5 + (245 - (scale * photo.width)) / 2), 285, { scale: scale });
    }
    if (this.exhibit_cnt == 4) {
      this.doc.image(photo.dataUrl, (320 + (245 - (scale * photo.width)) / 2), 285, { scale: scale });
    }
    if (this.exhibit_cnt == 5) {
      this.doc.image(photo.dataUrl, (this.left_margin - 5 + (245 - (scale * photo.width)) / 2), 490, { scale: scale });
    }
    if (this.exhibit_cnt == 6) {
      this.doc.image(photo.dataUrl, (320 + (245 - (scale * photo.width)) / 2), 490, { scale: scale });
    }

    this.exhibit_cnt++;

  }

  close() {
    var stream = this.doc.pipe(blobStream())
    this.doc.end();
    stream.on("finish", () => {
      var url = stream.toBlobURL('application/pdf');
      var blob = stream.toBlob('application/pdf');

      if (window.navigator && window.navigator.msSaveOrOpenBlob) {
        console.log("here");
        window.navigator.msSaveOrOpenBlob(blob, this.proj.name + "_" + new DatePipe("en-US").transform(new Date().getTime(), 'MMddyy') + ".pdf");
      }
      else {
        let a = document.createElement("a");
        a.download = "MoveIn_" + new DatePipe("en-US").transform(new Date().getTime(), 'MMddyy'); + ".pdf";
        a.href = url;
        a.click();
      }
    });
  }
}