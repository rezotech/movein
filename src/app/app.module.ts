import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { AngularFireModule } from '@angular/fire';
import { AngularFireAnalyticsModule } from '@angular/fire/analytics';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { environment } from '../environments/environment.prod';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { MatCardModule } from '@angular/material/card';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatInputModule } from '@angular/material/input';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatRadioModule } from '@angular/material/radio';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatBadgeModule } from '@angular/material/badge';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { PhotolistComponent } from './photos/photolist/photolist.component';
import { PhotograbComponent } from './photos/photograb/photograb.component';
import { LayoutModule } from '@angular/cdk/layout';


const routes: Routes = [
  { path: 'photos', component: PhotolistComponent },
  { path: '', component: PhotograbComponent },
];

@NgModule({
  imports: [
    BrowserModule,
    LayoutModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireAnalyticsModule,
    AngularFirestoreModule,
    RouterModule,
    FormsModule,
    MatCardModule,
    MatGridListModule,
    MatInputModule,
    MatIconModule,
    MatButtonModule,
    MatSlideToggleModule,
    MatRadioModule,
    MatBadgeModule,
    BrowserAnimationsModule,
    RouterModule.forRoot(routes, { enableTracing: false }),
  ],
  declarations: [AppComponent, PhotolistComponent, PhotograbComponent],
  bootstrap: [AppComponent],
})
export class AppModule {}
