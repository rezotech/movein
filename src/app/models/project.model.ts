import { Photo } from './photo.model';

export class Project {
    key: string;
    id: number;
    name: string = "The Project";
    number: string;
    photos: Photo[] = [];
    img_qual: number = .3;
    report_date: string;
  account_key: string;
    
}
