export class Photo {
    id: string;  
    dataUrl: string;
    description: string;
    name: string;
    no: string;
    width: number;
    height: number;
    scale: number;
    open_date: number;
    
    constructor() {
    }

}