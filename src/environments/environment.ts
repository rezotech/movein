// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyDVD1if26NyzilAVVCBDZnnRq1QglccAEI',
    authDomain: 'movein-acc9c.firebaseapp.com',
    projectId: 'movein-acc9c',
    storageBucket: 'movein-acc9c.appspot.com',
    messagingSenderId: '1057343561478',
    appId: '1:1057343561478:web:d5f332288630058a244887',
    measurementId: 'G-3Y63BMZBP3',
  },
  stripeKey: 'pk_test_C7x3Fbmh4pHzq6qnQXVaxCW300VPNnW6ge',
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
